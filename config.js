module.exports = {
  platform: "gitlab",
  endpoint: "https://gitlab.com/api/v4/",
  assignees: ["ywtstewart"],
  baseBranches: ["develop"],
  labels: ["renovate", "dependency update", "front-end"],
  extends: ["config:base"],
  hostRules: [
    {
      domainName: "github.com",
      encrypted: {
        token:
          "RGyQKi9GBYG2+y4F5kf6FktmZmOZreiLVJdrD/HUJuny6nbg9jmP87/dsfTem9uDCYIbl3FXa2rLLhb7mbd4AhMQkDUHLroWyZ6N+udqRpvXY8Jx9cIpXD/eloGeax3p/8qsnsHtTGaCIczSUisrXeZSvq/wZ+gO7V/Tv3jY+jSISgEWMAPZYVWv7IjzkjrSVxHYXLV+bt9yM0SpCcCVFc6tMqn6eY2ZgFMACJsKRHnWwAXlkFdJHAYg9eHMCvaGJB2QvAEsKWuWIrxTjP+UIKwomyMmxaPAnCUBETx4uvQM3Z9KzzDN94PWjcEbOO5mbA1Y6lkJmFIPOcW6NVui+A==",
      },
    },
  ],
};
